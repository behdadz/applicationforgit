package com.example.applicationforgit

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.Toast

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val messageBtn=findViewById<Button>(R.id.bnt_main)
        messageBtn.setOnClickListener {
            Toast.makeText(this,"Hello Git",Toast.LENGTH_SHORT).show()
        }
    }
}